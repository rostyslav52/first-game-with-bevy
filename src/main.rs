use bevy::prelude::*;
use map_plugin::resources::MapOptions;
use map_plugin::MapPlugin;
//TODO
/*
#[cfg(feature = "debug")]
use bevy_inspector_egui::WorldInspectorPlugin;
*/

fn main() {
    let mut app = App::new();

    app.insert_resource(WindowDescriptor {
        title: "Prologue".to_string(),
        width: 700.,
        height: 800.,
        ..Default::default()
    })
    .add_plugins(DefaultPlugins);
    //TODO
    /*
    #[cfg(feature = "debug")]
    app.add_plugin(WorldInspectorPlugin::new());
     */
    app.add_startup_system(camera_setup);
    app.insert_resource(MapOptions {
        map_size: (20, 20),
        tile_padding: 3.0,
        ..Default::default()
    });
    app.add_plugin(MapPlugin);
    app.run();
}

fn camera_setup(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}
