pub mod components;
pub mod resources;

use crate::resources::tile::Tile;
use bevy::log;
use bevy::prelude::*;
use components::Coordinates;
use resources::tile_map::TileMap;
use resources::MapOptions;
use resources::MapPosition;
use resources::TileSize;

pub struct MapPlugin;

impl Plugin for MapPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(Self::create_map);
        log::info!("Map Plugin loaded");
    }
}

impl MapPlugin {
    fn adaptative_tile_size(
        window: Res<WindowDescriptor>,
        (min, max): (f32, f32),      // Tile size constraints
        (width, height): (u16, u16), // Tile map dimensions
    ) -> f32 {
        let max_width = window.width / width as f32;
        let max_heigth = window.height / height as f32;
        max_width.min(max_heigth).clamp(min, max)
    }

    fn create_sprite_on_screen(
        tile_type: &Tile,
        options: &MapOptions,
        tile_size: f32,
        position: (usize, usize),
    ) -> SpriteBundle {
        SpriteBundle {
            sprite: Sprite {
                color: match tile_type {
                    Tile::Passable => Color::SEA_GREEN,
                    Tile::Impassable => Color::GRAY,
                },
                custom_size: Some(Vec2::splat(tile_size - options.tile_padding as f32)),
                ..Default::default()
            },
            transform: Transform::from_xyz(
                (position.0 as f32 * tile_size) + (tile_size / 2.),
                (position.1 as f32 * tile_size) + (tile_size / 2.),
                1.,
            ),
            ..Default::default()
        }
    }

    fn display_on_screen(
        mut commands: Commands,
        options: MapOptions,
        tile_map: TileMap,
        tile_size: f32,
        map_size: Vec2,
        map_position: Vec3,
    ) {
        let background_sprite = SpriteBundle {
            sprite: Sprite {
                color: Color::WHITE,
                custom_size: Some(map_size),
                ..Default::default()
            },
            transform: Transform::from_xyz(map_size.x / 2., map_size.y / 2., 0.),
            ..Default::default()
        };

        commands
            .spawn()
            .insert(Name::new("Map"))
            .insert(Transform::from_translation(map_position))
            .insert(GlobalTransform::default())
            .with_children(|parent| {
                parent
                    .spawn_bundle(background_sprite)
                    .insert(Name::new("Background"));
                for (y, line) in tile_map.iter().enumerate() {
                    for (x, tile) in line.iter().enumerate() {
                        parent
                            .spawn_bundle(Self::create_sprite_on_screen(
                                tile,
                                &options,
                                tile_size,
                                (x, y),
                            ))
                            .insert(Name::new(format!("Tile ({}, {})", x, y)))
                            // We add the `Coordinates` component to our tile entity
                            .insert(Coordinates {
                                x: x as u16,
                                y: y as u16,
                            });
                    }
                }
            });
    }

    pub fn create_map(
        mut commands: Commands,
        map_options: Option<Res<MapOptions>>,
        window: Res<WindowDescriptor>,
    ) {
        let options = match map_options {
            None => MapOptions::default(),
            Some(o) => o.clone(),
        };
        let mut tile_map = TileMap::create_empty(options.map_size.0, options.map_size.1);
        (*tile_map)[5][5] = Tile::Impassable; // TODO : remove
        let tile_size = match options.tile_size {
            TileSize::Fixed(v) => v,
            TileSize::Adaptive { min, max } => Self::adaptative_tile_size(
                window,
                (min, max),
                (tile_map.width(), tile_map.height()),
            ),
        };
        let map_size = Vec2::new(
            tile_map.width() as f32 * tile_size,
            tile_map.height() as f32 * tile_size,
        );
        log::info!("map size: {}", map_size);
        let map_position = match options.position {
            MapPosition::Centered { offset } => {
                Vec3::new(-(map_size.x / 2.), -(map_size.y / 2.), 0.) + offset
            }
            MapPosition::Custom(p) => p,
        };
        #[cfg(feature = "debug")]
        log::info!("{}", tile_map.console_output());
        Self::display_on_screen(
            commands,
            options,
            tile_map,
            tile_size,
            map_size,
            map_position,
        );
    }
}
