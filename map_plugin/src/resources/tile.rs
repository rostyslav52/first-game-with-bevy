#[cfg(feature = "debug")]
use colored::Colorize;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Tile {
    Passable,
    Impassable,
}

impl Tile {
    pub const fn is_walkable(&self) -> bool {
        matches!(self, Self::Passable)
    }

    #[cfg(feature = "debug")]
    pub fn console_output(&self) -> String {
        format!(
            "{}",
            match self {
                Tile::Impassable => "#".black(),
                Tile::Passable => " ".normal(),
            }
        )
    }
}
