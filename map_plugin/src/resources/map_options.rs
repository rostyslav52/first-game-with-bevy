use bevy::prelude::Vec3;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum TileSize {
    Fixed(f32),
    Adaptive { min: f32, max: f32 },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum MapPosition {
    Centered { offset: Vec3 },
    Custom(Vec3),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MapOptions {
    pub map_size: (u16, u16),
    pub position: MapPosition,
    pub tile_size: TileSize,
    pub tile_padding: f32,
}

impl Default for TileSize {
    fn default() -> Self {
        Self::Adaptive {
            min: 10.0,
            max: 50.0,
        }
    }
}

impl Default for MapPosition {
    fn default() -> Self {
        Self::Centered {
            offset: Default::default(),
        }
    }
}

impl Default for MapOptions {
    fn default() -> Self {
        Self {
            map_size: (15, 15),
            position: Default::default(),
            tile_size: Default::default(),
            tile_padding: 0.,
        }
    }
}
